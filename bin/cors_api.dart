import 'dart:convert';
import 'dart:io';

import 'package:shelf_plus/shelf_plus.dart';
import 'package:shelf/shelf_io.dart' as io;
import 'package:http/http.dart' as http;
import 'package:shelf_cors_headers/shelf_cors_headers.dart';

import 'api_cache.dart';
import 'my_request.dart';

const version = "1.7.4";
const identifier = "mwcors_server";

Future<void> runServer(int port, {String? cert, String? key}) async {
  Map<String, APICacheRegistry> getMap = {};
  final liturgyCache = APICache(duration: Duration(hours: 1));
  //final trendsCache = APICache(duration: Duration(hours: 1));
  final genericCache = APICache(duration: Duration(minutes: 5));
  final genericPostCache = APICache(duration: Duration(minutes: 5));
  final app = Router();

  app.get('/version', (Request request) {
    return Response.ok(version);
  });

  app.get('/id', (Request request) {
    return Response.ok(identifier);
  });

  app.get('/teapot', (Request request) {
    final body = """ 
        <html> <body> <a href = "https://menezesworks.com"> I am a teapot! </a> </body> </html>
    """;
    print(request.context);
    final x = request.context["shelf.io.connection_info"] as HttpConnectionInfo;
    print(x.remoteAddress.toString());
    x.remoteAddress.reverse().then((v) => print(v));

    return Response(418, body: body, headers: {"content-type": "text/html"});
  });

  app.get("/liturgy", (Request request) async {
    final result = await getJsonStringCached(liturgyUrl, liturgyCache);
    return Response.ok(result, headers: {"content-type": "text/json"});
  });

  app.get("/liturgy/<day>/<month>/<year>",
      (Request request, String dayS, String monthS, String yearS) async {
    final day = int.tryParse(dayS);
    final month = int.tryParse(monthS);
    final year = int.tryParse(yearS);
    if (day == null || month == null || year == null) {
      return Response.internalServerError(
          body: "Please use /liturgy/<day>/<month>/<year>");
    }
    final result = await getJsonStringCached(
        liturgyForDayUrl(day, month, year), liturgyCache);
    return Response.ok(result, headers: {"content-type": "text/json"});
  });

  app.get("/liturgy/<year>/<month>",
      (Request request, String yearS, String monthS) async {
    final month = int.tryParse(monthS);
    final year = int.tryParse(yearS);
    if (month == null || year == null) {
      return Response.internalServerError(
          body: "Please use /liturgy/<year>/<month>/");
    }
    final result = await getJsonStringCached(
        liturgyForMonthUrl(month, year), liturgyCache);
    return Response.ok(result, headers: {"content-type": "text/json"});
  });

  app.get("/liturgicalCalendar/<year>", (Request request, String yearS) async {
    final year = int.tryParse(yearS);
    if (year == null) {
      return Response.internalServerError(
          body: "Please use /liturgicalCalendar/<year>");
    }
    final result =
        await getJsonStringCached(liturgicalCalendarUrl(year), liturgyCache);
    return Response.ok(result, headers: {"content-type": "text/json"});
  });

  app.post("/json/text", (Request request) async {
    final obj = await request.body.asJson;
    final str = obj.toString();
    print(str);
    final userRequest = ProxyHttpRequest.fromJson(obj);
    final text = userRequest.body["text"];
    final result = await http.post(Uri.parse(userRequest.url), body: text);
    return Response(result.statusCode,
        body: result.body, headers: {"content-type": "text/text"});
  });

  app.post("/compile", (Request request) async {
    final base = 
        false ? "http://menezesworks.com:4000" : "http://localhost:3000";
    final bodyStr = await request.body.asString;
    if (bodyStr.length > 1024 * 1024) {
      return Response.internalServerError(body: "Error: Program too big");
    }
    try {
      final response =
          await http.post(Uri.parse("$base/hac/compile"), body: bodyStr);

      print(response.headers);
      return Response(response.statusCode,
          body: response.body, headers: response.headers); //use same headers as response
    } catch (e) {
      return Response.notFound("Server unreachable. Contact admin $e");
    }
  });

  app.post('/json', (Request request) async {
    final obj = await request.body.asJson;
    final str = obj.toString();
    print(str);
    final userRequest = ProxyHttpRequest.fromJson(obj);
    print(userRequest.headers);
    if (userRequest.method.toLowerCase() == "get") {
      final headers = userRequest.headers
          .map<String, String>((key, value) => MapEntry(key, value.toString()));
      if (userRequest.shouldCache == true) {
        final result = await getJsonStringCached(userRequest.url, genericCache,
            headers: headers);
        return Response.ok(result, headers: {"content-type": "text/json"});
      } else {
        final result = await getJsonString(userRequest.url, headers: headers);
        return Response.ok(result, headers: {"content-type": "text/json"});
      }
    } else if (userRequest.method.toLowerCase() == "post") {
      final headers = userRequest.headers
          .map<String, String>((key, value) => MapEntry(key, value.toString()));
      if (userRequest.shouldCache == true) {
        final result = await postJsonStringCached(
            userRequest.url, userRequest.body, genericPostCache,
            headers: headers);
        return Response.ok(result, headers: {"content-type": "text/json"});
      } else {
        final result = await postJsonString(userRequest.url, userRequest.body,
            headers: headers);
        return Response.ok(result, headers: {"content-type": "text/json"});
      }
    } else if (userRequest.method.toLowerCase() == "patch") {
      final headers = userRequest.headers
          .map<String, String>((key, value) => MapEntry(key, value.toString()));
      if (userRequest.shouldCache == true) {
        final result = await patchJsonStringCached(
            userRequest.url, userRequest.body, genericPostCache,
            headers: headers);
        return Response.ok(result, headers: {"content-type": "text/json"});
      } else {
        final result = await patchJsonString(userRequest.url, userRequest.body,
            headers: headers);
        return Response.ok(result, headers: {"content-type": "text/json"});
      }
    } else if (userRequest.method.toLowerCase() == "delete") {
      final headers = userRequest.headers
          .map<String, String>((key, value) => MapEntry(key, value.toString()));
      if (userRequest.shouldCache == true) {
        final result = await deleteJsonStringCached(
            userRequest.url, userRequest.body, genericPostCache,
            headers: headers);
        return Response.ok(result, headers: {"content-type": "text/json"});
      } else {
        final result = await deleteJsonString(userRequest.url, userRequest.body,
            headers: headers);
        return Response.ok(result, headers: {"content-type": "text/json"});
      }
    }
    return Response.ok("Survived $str");
    //var person = Schedule.fromJson(await request.body.asJson);
    //return 'You send me: ${person.name}';
  });

  app.get("/getjson/<url>", (Request request, String url) async {
    print(url);
    final uri = Uri.parse("https://$url");
    // return Response.ok(uri.toString());
    final obj = getMap[uri.toString()];
    if (obj == null || cacheTimeout(obj, Duration(minutes: 10))) {
      final result = await getJsonString(uri.toString());
      return Response.ok(result, headers: {"content-type": "text/json"});
    } else {
      print("has resp");
      http.Response resp = obj.response;
      return Response.ok(resp.body);
      //return Response(resp.statusCode, body: resp.body, headers: resp.headers);
    }
  });
  final security = SecurityContext.defaultContext;

  final overrideHeaders = {
    ACCESS_CONTROL_ALLOW_ORIGIN: '*',
    ACCESS_CONTROL_ALLOW_METHODS: 'GET,POST,PUT,PATCH,DELETE',
    'Content-Type': 'application/json;charset=utf-8'
  };

  final handler = Pipeline()
      .addMiddleware(corsHeaders(headers: overrideHeaders))
      .addHandler(app);

  if (cert != null && key != null) {
    security.useCertificateChain(cert);
    security.usePrivateKey(key);
  }
  if (debug) {
    final server = await io.serve(handler, 'localhost', port);
    print("DEBUG MODE");
  } else {
    final server = await io.serve(handler,
        useIpv6 ? InternetAddress.anyIPv6 : InternetAddress.anyIPv4, port,
        securityContext: security);
    final serverDebug = await io.serve(handler, 'localhost', port + 1);
  }

  print('Server created!');
}

bool cacheTimeout(APICacheRegistry reg, Duration time) {
  return DateTime.now().difference(reg.timestamp) > time;
}

void main(List<String> arguments) {
  const defaultPort = 8080;
  int port = defaultPort;
  String? certificate;
  String? key;
  if (arguments.isNotEmpty) {
    if (arguments.contains("--help")) {
      help();
    }
    if (arguments.contains("--version")) {
      displayVersion();
    }
    if (arguments.contains("--debug")) {
      debug = true;
    }
    if (arguments.contains("--ipv6")) {
      useIpv6 = true;
    }
    port = int.tryParse(arguments[0]) ?? defaultPort;
  }
  if (arguments.length >= 3) {
    final certPath = arguments[1];
    final keyPath = arguments[2];

    certificate = certPath;
    key = keyPath;

    runServer(port, cert: certificate, key: key).then((_) {
      print('Server Online https! $port');
    });
  } else {
    runServer(port).then((_) {
      print('Server Online! $port');
    });
  }
}

void displayVersion() {
  print("$identifier $version");
  exit(1);
}

void help() {
  print("for http:");
  print("\tusage: ./cors_api.exe [port] [flags] ");
  print("for https:");
  print("\tusage: ./cors_api.exe [port] [certificate_path] [key_path] [flags]");
  print("flags:");
  print("\tuse: --debug to enable localhost");
  print("\tuse: --ipv6 to enable ipv6");
  print("\tuse: --help to display this message");
  exit(1);
}

final String trendsUrl = "https://trends.gab.com/trend-feed/json";
final String liturgyUrl =
    "http://calapi.inadiutorium.cz/api/v0/en/calendars/default/today";
String liturgyForDayUrl(int day, int month, int year) =>
    "http://calapi.inadiutorium.cz/api/v0/en/calendars/default/$year/$month/$day";
String liturgyForMonthUrl(int month, int year) =>
    "http://calapi.inadiutorium.cz/api/v0/en/calendars/default/$year/$month/";
String liturgicalCalendarUrl(int year) =>
    "http://calapi.inadiutorium.cz/api/v0/en/calendars/default/$year";

late bool debug = false;
late bool useIpv6 = false;

Future<String> getJsonStringCached(String str, APICache cache,
    {Map<String, String>? headers}) async {
  final response = await cache.get(str, headers: headers);
  if (response.statusCode != 200) {
    cache.clearGetEntry(str); //don't cache weird results
  }
  return response.body;
}

Future<String> getJsonString(String str, {Map<String, String>? headers}) async {
  final url = Uri.parse(str);
  final response = await http.get(url, headers: headers);
  return response.body;
}

Future<String> postJsonString(String str, Object? body,
    {Map<String, String>? headers}) async {
  final url = Uri.parse(str);
  final response = await http.post(url, body: body, headers: headers);
  return response.body;
}

Future<String> postJsonStringCached(String str, Object? body, APICache cache,
    {Map<String, String>? headers}) async {
  final JsonEncoder encoder = JsonEncoder();
  final encoded = encoder.convert(body);
  final response = await cache.post(str, encoded, headers: headers);
  if (response.statusCode != 200) {
    cache.clearPostEntry(str, encoded); //don't cache weird results
  }
  return response.body;
}

Future<String> postString(String str, String? body,
    {Map<String, String>? headers}) async {
  final url = Uri.parse(str);
  final response = await http.post(url, headers: headers, body: body);
  return response.body;
}

Future<String> patchJsonStringCached(String str, Object? body, APICache cache,
    {Map<String, String>? headers}) async {
  final JsonEncoder encoder = JsonEncoder();
  final encoded = encoder.convert(body);
  final response = await cache.patch(str, encoded, headers: headers);
  if (response.statusCode != 200) {
    cache.clearPostEntry(str, encoded); //don't cache weird results
  }
  return response.body;
}

Future<String> patchString(String str, String? body,
    {Map<String, String>? headers}) async {
  final url = Uri.parse(str);
  final response = await http.patch(url, headers: headers, body: body);
  return response.body;
}

Future<String> patchJsonString(String str, Object? body,
    {Map<String, String>? headers}) async {
  final url = Uri.parse(str);
  final jstr = jsonEncode(body);
  print(jstr);
  final response = await http.patch(url, body: jstr, headers: headers);
  return response.body;
}

Future<String> deleteString(String str, String? body,
    {Map<String, String>? headers}) async {
  final url = Uri.parse(str);
  final response = await http.delete(url, headers: headers, body: body);
  return response.body;
}

Future<String> deleteJsonString(String str, Object? body,
    {Map<String, String>? headers}) async {
  final url = Uri.parse(str);
  final jstr = jsonEncode(body);
  print(jstr);
  final response = await http.delete(url, body: jstr, headers: headers);
  return response.body;
}

Future<String> deleteJsonStringCached(String str, Object? body, APICache cache,
    {Map<String, String>? headers}) async {
  final JsonEncoder encoder = JsonEncoder();
  final encoded = encoder.convert(body);
  final response = await cache.delete(str, encoded, headers: headers);
  if (response.statusCode != 200) {
    cache.clearPostEntry(str, encoded); //don't cache weird results
  }
  return response.body;
}
