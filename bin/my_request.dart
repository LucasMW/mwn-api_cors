import 'dart:convert';

enum Method { get, post, patch, delete }

class ProxyHttpRequest {
  ProxyHttpRequest(
      {required this.method,
      required this.url,
      required this.body,
      required this.headers,
      this.shouldCache = true});
  late final String method;
  late final String url;
  late final Map<String, dynamic> body;
  late final Map<String, dynamic> headers;
  bool shouldCache = true;
  bool itse = false;

  ProxyHttpRequest.fromJson(Map<String, dynamic> json) {
    method = json['method'];
    url = json['url'];
    body = json['body'] ?? {};
    headers = json['headers'] ?? {};
    shouldCache = json['cache'] ?? shouldCache;
    itse = json['itse'] ?? itse;
    if (itse == true) {
      print("It is a secret to everybody");
    }
  }

  void decypher(String x) {
    final str = body['data'] as String;
    
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['method'] = method;
    _data['url'] = url;
    _data['body'] = body;
    _data['headers'] = headers;
    _data['cache'] = shouldCache;
    _data['itse'] = itse;
    return _data;
  }
}
